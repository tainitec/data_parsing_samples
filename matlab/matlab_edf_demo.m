clear; close all;

file_name = '../test_signals/TAINI_A001_-2020_12_02-0000.edf';

[header, signal_header, signal_cells] = blockEdfLoad(fullfile(file_name));
[event_times, event_values, event_binary] = parse_edf_plus_annotations(signal_header, signal_cells );

% Calculate sample frequency of channel one in EDF file
sample_frequency = signal_header(1).samples_in_record / header.data_record_duration;
ploss_symbol = 0.0;

% Plot
signal = cell2mat({signal_cells{1:16}})';
plot_recording(signal, sample_frequency, ploss_symbol, event_values, event_times );
