function [ event_times, event_values, event_binary ] = parse_edf_plus_annotations( signal_header, signal_cells )
%parse_taini_edf_annotations converts the annotations in a TAINI edf file
%into sync event values and time stamps

%% Extract the annotations cells (first 16 are channel values)
annotation_cells = signal_cells(17:end);
annotation_events_all = [];

%% Parse individual events
for idx = 1:length(annotation_cells)

    annotation_uint16 = round(annotation_cells{idx} * signal_header(end).digital_max);
    annotation_uint8_lower = bitand(annotation_uint16, (2^8-1));
    annotation_uint8_upper = bitand(bitshift(annotation_uint16,-8), (2^8-1));
    annotation_uint8 = reshape( [annotation_uint8_lower, annotation_uint8_upper]', length(annotation_uint8_lower) * 2, 1);
    annotation_chars = char(annotation_uint8);
    n_chars_per_event = signal_header(end).samples_in_record * 2;
    n_events = length(annotation_chars) / n_chars_per_event;
    annotation_events = reshape(annotation_chars', n_chars_per_event, n_events)';
    annotation_events_all = [char(annotation_events_all); char(annotation_events)];

end


%% For each event convert format
event_times = [];
event_values = [];
n_events = length(annotation_events_all);
for idx = 1:n_events

    % Get the event
    ev = annotation_events_all(idx,:);

    % Skip if no visible chars
    if (max(ev) == 0)
        continue;
    end

    % Remove 0s
    ev2 = strrep(ev, char(0), '');

    % Split at 20s
    ev3 = strsplit(ev2, char(20));

    % Get the time and sync values
    if length(ev3) == 3
        ev_t = ev3{1};
        ev_v = ev3{2};
    elseif length(ev3) == 4
        ev_t = ev3{2};
        ev_v = ev3{3};
    else
        continue;
    end

    % Convert value
    if ~strcmp(ev_v(1:5), 'SYNC_')
        continue;
    end
    ev_int = str2num(ev_v(6:end));

    % Set output
    event_times = [event_times, str2num(ev_t)];
    event_values = [event_values, ev_int];

end


%% Sort the outputs
[event_times, sort_idx] = sort(event_times);
event_values = event_values(sort_idx);


%% Convert to binary (only supported in MATLAB, not Octave)
if exist('OCTAVE_VERSION', 'builtin') ~= 0
    event_binary = [];
else
    event_binary = boolean(de2bi(event_values, 12, 'left-msb'));
end


end
