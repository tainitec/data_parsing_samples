function val = parse_yaml(file_name, key_list, to_double=false)
%% MATLAB doesn't include a yaml parser, so this is a very simple function that
% will work with TAINILIVE yaml files, but does not include error checking


hier_path = {};

fid = fopen(file_name, 'rt');
while ~feof(fid)
    tline = fgetl(fid);

    matched_names = regexp(tline, '^(?<wsp>\s*)(?<lst>-*\s*)(?<tag>\w+):\W*(?<val>\S*.*)', 'names');
    hier_depth = (size(matched_names.wsp,2) / 2) + 1;

    % Process if it is a list and value
    if length(matched_names.lst) ~= 0
        if length(hier_path) >= hier_depth && isa(hier_path{hier_depth}, 'double')
            hier_path{hier_depth} = round(hier_path{hier_depth}) + 1;
        else
            hier_path{hier_depth} = 0;
        end
        hier_depth += 1;
    end

    hier_path{hier_depth} = matched_names.tag;
    hier_path = {hier_path{1:hier_depth}};

    % Check if the required key list matched the current hierarchy path
    % Effectively this is: if hier_path == key_list
    if length(hier_path) == length(key_list)
        all_match = true;
        for idx = 1:length(hier_path)
            if length(hier_path{idx}) ~= length(key_list{idx}) || ...
                    class(hier_path{idx}) ~= class(key_list{idx}) || ...
                    hier_path{idx} ~= key_list{idx}
                all_match = false;
            end
        end
        if all_match
            val = matched_names.val;
            if to_double
                val = str2double(val);
            end
            return;
        end
    end

    val = NaN;

end
fclose(fid);

end
