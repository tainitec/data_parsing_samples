function plot_recording(signals, sample_frequency, ploss_symbol, event_values, event_times, ch=[])
%% Plot the signals or a subset of them
%
% Inputs:
%    signals : signal data from transmitter channels as a list of lists
%    sample_frequency : the nominal sample frequency for all signal channels
%    ploss_symbol : symbol used in the signal data to identify packet loss
%    event_value : list of event synchronisation values
%    event_times : list of times for the synchronisation events
%    ch : the indices of channels to plot (plot all by default)

% Default argument inputs
[nchan, ns] = size(signals);
if length(ch) == 0
    ch = 1:nchan;
end
signals = signals(ch,:);
[nchan, ns] = size(signals);

% Generate the time array for plotting
t = (0:(ns-1)) / sample_frequency;

% Remove packet loss from the plot
t = t(signals(1,:) ~= ploss_symbol);
signals_nopl = [];
for idx = 1:nchan
    signals_nopl(idx,:) = signals(idx, signals(idx,:) ~= ploss_symbol);
end

% Compute RMS of signals to estimate a visually pleasing vertical offset
vertical_offset = 4 * max(rms(signals_nopl-mean(signals_nopl,2),2)');
signals_nopl_shifted = signals_nopl - mean(signals_nopl,2) + ((nchan:-1:1)' * vertical_offset);

% Plot channels, normalised around zero and shifted vertically
% (top is ch0, bottom is chmax)
figure;
[AX,H1,H2] = plotyy(event_times, event_values, t, signals_nopl_shifted);
title('Signal and Events Against Time (in seconds)');
xlabel('Recording Time (seconds)');
ylabel(AX(1),'Events');
ylabel(AX(2),'Signal');

end
