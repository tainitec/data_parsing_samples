clear; close all;

file_name = '../test_signals/TAINI_A001_-2020_12_02-0001.dat';

% Infer the sync and configuration yaml filepath
file_name_sync = strcat(file_name(1:end-4), '.sync');
file_name_config = strcat(file_name(1:end-4), '_configuration.yaml');
disp(strcat('Parsing dat file "', file_name, '"'));
disp(strcat('        sync file "', file_name_sync, '"'));
disp(strcat('        config file "', file_name_config, '"'));

% Process configuration yaml
nominal_sample_frequency = parse_yaml(file_name_config, {'transmitters',0,'nominal_sample_frequency'}, true);
decimation = parse_yaml(file_name_config, {'transmitters',0,'decimation'}, true);
sample_frequency = nominal_sample_frequency / decimation;
ploss_symbol = parse_yaml(file_name_config, {'recording','lost_data_symbol'}, true);
no_channels = parse_yaml(file_name_config, {'transmitters',0,'no_channels'}, true);
bit_depth = parse_yaml(file_name_config, {'transmitters',0,'bit_depth'}, true);
mvolt_range = parse_yaml(file_name_config, {'transmitters',0,'mvolt_range'}, true);

% Demultiplex dat file into channels
int2mV = mvolt_range / (2 ** bit_depth - 1);
ploss_symbol *= int2mV;
fileID = fopen(file_name,'r');
dat_raw = fread(fileID,'uint16');
fclose(fileID);
signals = reshape(dat_raw,no_channels,length(dat_raw)/no_channels) * int2mV;

% Extract synchronisation events
event_values = [];
event_times = [];
fid = fopen(file_name_sync, 'rt');
while ~feof(fid)
    tline = fgetl(fid);
    matched_names = regexp(tline, '(?<ts>\d+) SYNC_(?<val>\d+)', 'names');
    if length(matched_names) > 0
        event_values(end+1) = str2double(matched_names.val);
        event_times(end+1) = str2double(matched_names.ts) / sample_frequency;
    end
end
fclose(fid);

% Plot
plot_recording(signals, sample_frequency, ploss_symbol, event_values, event_times );
