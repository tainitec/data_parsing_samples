# TAINILIVE Output Data File Format Parsing Samples
This repository contains sample code for parsing TAINILIVE recordings in MATLAB/Octave and Python.

## MATLAB (untested)/Octave (v6.1.0)
### European Data Format EDF+ (.edf)
[`matlab_edf_demo.m`](./matlab/matlab_edf_demo.m)

This script demonstrates parsing EDF+ files with the open-source function `blockEdfLoad` available from the MATLAB file exchange [here](https://mathworks.com/matlabcentral/fileexchange/42784-blockedfload).

Unfortunately, `blockEdfLoad` does not support the full EDF+ format that includes annotations. Therefore, if you are using TTL synchronisation, we have created a sample function `parse_edf_plus_annotations` that can convert the annotations into a list of synchronisation events.

*NOTE: `blockEdfLoad` is not included in this repository, you will need to independently download it from the MATLAB file exchange and put it in the `matlab/edf` directory.*

### TAINILIVE Binary Data Format (.dat)
[`matlab_dat_demo.m`](./matlab/matlab_dat_demo.m)

This script demonstrates parsing dat files with pure MATLAB/Octave. As MATLAB and Octave do not have an easy open-source YAML parser for interpreting the configuration YAML file, therefore a simple parser based on a regex command has been included.

## Python (v3.6)
A [`requirements.txt`](./python/requirements.txt) file is included in the python repository with the tested `pip` library versions.

### European Data Format EDF+ (.edf)
[`python_edf_demo.m`](./python/python_edf_demo.py)

This script demonstrates parsing EDF+ files with the open-source library `PyEDFlib`. This is a python wrapper for the widely used `C++` library `EDFlib`.

`PyEDFlib` is available [here](https://pyedflib.readthedocs.io/en/latest/) and it can be installed with the python package manager `pip`.

### TAINILIVE Binary Data Format (.dat)
[`python_dat_demo.m`](./python/python_dat_demo.py)

This script demonstrates parsing dat files with pure python. This function uses the python YAML parser `PyYAML`.

`PyYAML` is available [here](https://github.com/yaml/pyyaml) and it can be installed with the python package manager `pip`.

## R (v4.0.3)
### European Data Format EDF+ (.edf)
Coming soon

### TAINILIVE Binary Data Format (.dat)
[`r_dat_demo.R`](./r/r_dat_demo.R)

This script demonstrates parsing dat files with pure R. This function uses the R YAML parser `libyaml`.

`libyaml` is available [here](https://rdrr.io/cran/yaml/) and it can be installed from The Comprehensive R Archive Network (CRAN) with the following command in R `install.packages("yaml")`.

## Test Signal Files
This [directory](./test_signals) contains both an EDF+ and a Binary Data Format recording with sinusoidal data. The raw frequency signals (at 19541 Hz samplerate) are as follows:

| Channel | Frequency | Channel | Frequency | Channel | Frequency | Channel | Frequency |
| ---: | :--- | ---: | :--- | ---: | :--- | ---: | :--- |
| 1 | 1 Hz | 5 | 10 Hz | 9 | 100 Hz | 13 | 1000 Hz |
| 2 | 1.8 Hz | 6 | 18 Hz | 10 | 180 Hz | 14 | 1800 Hz |
| 3 | 3.2 Hz | 7 | 32 Hz | 11 | 320 Hz | 15 | 3200 Hz |
| 4 | 5.6 Hz | 8 | 56 Hz | 12 | 560 Hz | 16 | 5600 Hz |


Note, the file was recorded using the online downsampling option to generate a file with a bandwidth of 500 Hz and a samplerate of 1085 Hz. Subsequently, all channels above 500 Hz have been low-passed by the anti-aliasing filter. The directory also contains the associated configuration.yaml and .sync files for the recording.

An image of the how these signals should look when plotted, along with 1 Hz synchronisation events in red is shown below.

![picture](test_signals/Figure_1.png)
![picture](test_signals/Figure_2.png)
