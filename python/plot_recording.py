import numpy as np
import matplotlib.pyplot as plt


def plot_recording(signals, sample_frequency, ploss_symbol, event_value, event_times, ch=None):
    '''Plot the data in a matplotlib figure

    Keyword arguments:
        signals -- signal data from transmitter channels as a list of lists
        sample_frequency -- the nominal sample frequency for all signal channels
        ploss_symbol -- symbol used in the signal data to identify packet loss
        event_value -- list of event synchronisation values
        event_times -- list of times for the synchronisation events
        ch -- the indices of channels to plot (plot all by default)
    '''

    # Default argument inputs
    nchan = len(signals)
    if ch == None:
        ch = range(nchan)

    # Generate the time array for plotting
    t = np.arange(len(signals[0])) / sample_frequency

    # Remove packet loss from the plot
    t = t[signals[0] != ploss_symbol]
    for idx in range(nchan):
        signals[idx] = signals[idx][signals[idx] != ploss_symbol]

    # Compute RMS of signals to estimate a visually pleasing vertical offset
    rms = [np.sqrt(np.mean((s-np.mean(s))**2)) for s in signals]
    vertical_offset = 4 * max(rms)

    # Plot channels, normalised around zero and shifted vertically
    # (top is ch0, bottom is chmax)
    fig, ax1 = plt.subplots()
    ax1.set_zorder(10)
    ax1.patch.set_visible(False)
    for c, idx in zip(ch, range(len(ch))[::-1]):
        plot_sig = signals[c] - np.mean(signals[c]) + vertical_offset * idx
        ax1.step(t, plot_sig, color='b', where='post')

    # Plot events
    ax2 = ax1.twinx()
    ax2.step(event_times, event_value, color='r', where='post')

    # Configure plot
    ax1.set(xlabel='Time (s)', ylabel='Relative voltage (mV)')
    ax2.set(ylabel='Synchronisation events')
    plt.show()
