#!/usr/bin/python3

import sys
import numpy as np
import yaml
import re
import plot_recording


def main(file_name):
    '''Demo script to parse a TAINILIVE dat file and plot it'''
    signals, sample_frequency, ploss_symbol, event_values, event_times = load_dat(file_name)
    plot_recording.plot_recording(signals, sample_frequency, ploss_symbol, event_values, event_times)


def load_dat(file_name):
    ''' Load a .dat file by interpreting it as uint16 and then de-interlacing
        the 16 channels.
        Function assumes that a .sync and a configuration.yaml are in the same
        directory with a matching name.
    '''

    # Infer the sync and configuration yaml filepath
    file_name_sync = file_name[:-4] + '.sync'
    file_name_config = file_name[:-4] + '_configuration.yaml'

    print ('Parsing dat file \"' + file_name + '\"')
    print ('        sync file \"' + file_name_sync + '\"')
    print ('        config file \"' + file_name_config + '\"')

    # Parse configuration and sync files
    with open(file_name_config, 'r') as yaml_stream:
        data_loaded = yaml.safe_load(yaml_stream)
    with open(file_name_sync, 'r') as sync_file:
        sync_file_lines = sync_file.readlines()

    # Process configuration yaml
    nominal_sample_frequency = data_loaded['transmitters'][0]['nominal_sample_frequency'];
    decimation = data_loaded['transmitters'][0]['decimation'];
    sample_frequency = nominal_sample_frequency / decimation
    ploss_symbol = data_loaded['recording']['lost_data_symbol']
    no_channels = data_loaded['transmitters'][0]['no_channels'];
    bit_depth = data_loaded['transmitters'][0]['bit_depth'];
    mvolt_range = data_loaded['transmitters'][0]['mvolt_range'];

    # Demultiplex dat file into channels
    int2mV = mvolt_range / (2 ** bit_depth - 1)
    ploss_symbol *= int2mV
    dat_raw = np.fromfile(file_name, dtype='uint16')
    signals = [dat_raw[c::no_channels] * int2mV for c in range(no_channels)]

    # Extract synchronisation events
    event_values = []
    event_times = []
    for sfl in sync_file_lines:
        m = re.search('(\d+) SYNC_(\d+)', sfl)
        if m:
            event_times.append(int(m.group(1)) / sample_frequency)
            event_values.append(m.group(2))

    return signals, sample_frequency, ploss_symbol, event_values, event_times


if __name__ == "__main__":

    # Use the default file if none are input
    if(len(sys.argv) == 1):
        file_name = '../test_signals/TAINI_A001_-2020_12_02-0001.dat'
    else:
        file_name = sys.argv[1]
    main(file_name)
