#!/usr/bin/python3

import sys
import numpy as np
import pyedflib
import plot_recording


def main(file_name):
    '''Demo script to parse a TAINILIVE EDF+ file and plot it'''
    signals, sample_frequency, ploss_symbol, event_values, event_times = load_edf(file_name)
    plot_recording.plot_recording(signals, sample_frequency, ploss_symbol, event_values, event_times)


def load_edf(file_name):
    '''Load an EDF+ file using pyEDFlib and return the signals'''

    print ('Loading EDF+ file \"' + file_name + '\"')
    f = pyedflib.EdfReader(file_name)
    ploss_symbol = 0.0

    print ('  - number of channels = ' + str(f.signals_in_file))
    print ('  - sample frequency = ' + str(f.samplefrequency(0)))
    print ('  - packet loss symbol = ' + str(ploss_symbol))

    signals = [f.readSignal(idx) for idx in range(f.signals_in_file)]
    sample_frequency = f.samplefrequency(0)
    event_values = [int(sync[5:]) for sync in f.readAnnotations()[2]]
    event_times = f.readAnnotations()[0]
    return signals, sample_frequency, ploss_symbol, event_values, event_times


if __name__ == "__main__":

    # Use the default file if none are input
    if(len(sys.argv) == 1):
        file_name = '../test_signals/TAINI_A001_-2020_12_02-0000.edf'
    else:
        file_name = sys.argv[1]
    main(file_name)
