library(yaml)  # install.packages("yaml")
library(stringr)  # install.packages("stringr")


load_dat <- function(file_name) {

  # Infer the sync and configuration yaml filepath
  file_name_sync <- paste(substr(file_name, 1, nchar(file_name)-4), '.sync', sep="")
  file_name_config <- paste(substr(file_name, 1, nchar(file_name)-4), '_configuration.yaml', sep="")
  cat(paste('Parsing dat file "', file_name, '"\n', sep=""))
  cat(paste('        sync file "', file_name_sync, '"\n', sep=""))
  cat(paste('        config file "', file_name_config, '"\n', sep=""))

  # Load the YAML (suppressing warning)
  defaultW <- getOption("warn")
  options(warn = -1)
  config_yaml <- read_yaml(file_name_config)
  options(warn = defaultW)
  
  # Process configuration yaml
  nominal_sample_frequency <- config_yaml$transmitters[[1]]$nominal_sample_frequency
  decimation <- config_yaml$transmitters[[1]]$decimation
  sample_frequency <- nominal_sample_frequency / decimation
  ploss_symbol <- config_yaml$recording$lost_data_symbol
  no_channels <- config_yaml$transmitters[[1]]$no_channels
  bit_depth <- config_yaml$transmitters[[1]]$bit_depth
  mvolt_range <- config_yaml$transmitters[[1]]$mvolt_range

  # Demultiplex dat file into channels
  int2mV <- mvolt_range / (2 ** bit_depth - 1)
  ploss_symbol <- ploss_symbol * int2mV
  nsamples <- file.info(file_name)$size / 2
  dat_file <- file(file_name, 'rb')
  dat_raw <- readBin(dat_file, integer(), n=nsamples, size=2, signed=FALSE)
  close(dat_file)
  signals <- matrix(dat_raw, nrow=no_channels) * int2mV

  # Extract synchronisation events
  event_values = list()
  event_times = list()
  event_file <- file(file_name_sync, 'r')
  while ( length(line) != 0 ) {
    line <- readLines(event_file, n = 1)
    line_regex <- stringr::str_match(line, '(\\d+) SYNC_(\\d+)')
    if (!is.na(line_regex[1])) {
      event_times[[length(event_times)+1]] <- as.integer(line_regex[2]) / sample_frequency
      event_values[[length(event_values)+1]] <- as.integer(line_regex[3])
    }
  }
  close(event_file)

  return(list(
    'signals'=signals,
    'sample_frequency'=sample_frequency,
    'ploss_symbol'=ploss_symbol,
    'event_values'=event_values,
    'event_times'=event_times
    ))
}
