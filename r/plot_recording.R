plot_recording <- function(signals, sample_frequency, ploss_symbol, event_times, event_values, ch=NULL) {
  
  # Extract first one second for plotting
  if (dim(signals)[2] > 2 * sample_frequency) {
    print('Only plotting first two seconds of the data')
    signals <-signals[,1:floor(2*sample_frequency)]
    event_values = event_values[event_times<=2.0]
    event_times = event_times[event_times<=2.0]
  }
  
  # Default argument inputs
  if (is.null(ch)) {
    nchan <- dim(signals)[1]
    ch <- 1:nchan
  }
  signals <- signals[ch,]
  nchan <- dim(signals)[1]
  
  # Generate the time array for plotting
  nsamp <- dim(signals)[2]
  t <- (0:(nsamp-1)) / sample_frequency

  # Remove packet loss from the plot
  t <- t[signals[1,] != ploss_symbol]
  signals <- signals[,signals[1,] != ploss_symbol]

  # Compute RMS of signals to estimate a visually pleasing vertical offset
  signals_normd <- apply(signals, 2, '-', rowMeans(signals))
  rms <- sqrt(rowMeans(signals_normd^2))
  vertical_offset <- 4 * max(rms)

  # Plot channels, normalised around zero and shifted vertically
  # (top is ch0, bottom is chmax)
  signals_offset = apply(signals_normd, 2, '-', vertical_offset * 1:nchan)
  matplot(t, t(signals_offset), type="l", col="blue", lty = 1, main="Signal Against Time (in seconds)", xlab="Recording Time (seconds)", ylab="Signal")
  
  # # Plot events
  # par(new = TRUE)
  # plot(event_times, event_values, type="l", col="red", axes=FALSE)
  
}

